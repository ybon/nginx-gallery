deploy:
	scp style.xslt ferment:/srv/diapo/
	rsync --checksum --rsync-path="sudo rsync" diapo.conf ferment:/etc/nginx/sites-enabled/
	ssh ferment sudo systemctl reload nginx
push:
ifdef ALBUM
	zip $(ALBUM)album.zip -j $(ALBUM)/* -x album.zip
	scp -r $(ALBUM) ferment:/srv/diapo/albums/
else
	echo "Missing ALBUM env var. Stop."
endif
