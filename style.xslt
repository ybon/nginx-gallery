<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="utf-8" indent="yes" />
<xsl:template match="/">
    <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
    <html>
    <head>
        <title><xsl:value-of select="$title" /></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <style>
        img {
            display: block;
            max-width: 90%;
            margin: 5%;
            max-height: 100vh;
            vertical-align: bottom;
            image-orientation: from-image;
            margin-left: auto;
            margin-right: auto;
        }
        body {
            margin: 0;
            background-color: #343b49;
            text-align: center;
        }
        .button {
            background-color: #434c5e;
            display: inline-block;
            color: #eee;
            padding: 2em;
            font-size: 2em;
            margin-bottom: 4em;
            text-decoration: none;
        }
        .button:hover {
            background-color: #3b4252;
        }
        </style>
    </head>
    <body>
        <xsl:for-each select="list/file">
            <xsl:choose>
                <xsl:when test="contains(' mp4 webm mkv avi wmv flv ogv ', concat(' ', substring-after(., '.'), ' '))">
                    <video controls="" src="{.}" alt="{.}" title="{.}"/>
                </xsl:when>
                <xsl:when test="contains(' zip ', concat(' ', substring-after(., '.'), ' '))">
                    <a href="{.}" class="button">Tout télécharger</a>
                </xsl:when>
                <xsl:otherwise>
                    <img src="{.}" alt="{.}" title="{.}"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </body>
    </html>
</xsl:template>
</xsl:stylesheet>
